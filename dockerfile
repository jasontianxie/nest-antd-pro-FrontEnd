# 先获取nodejs镜像，并给这个环境命名为nodeImage
FROM node:10.15.3 as nodeImage
# 在nodejs镜像中创建一个文件夹
RUN mkdir -p /var/www/frontEnd/
# 将当前目录（这个点表示当前目录下的所有内容，什么是“当前目录”呢？当CI/CD（gitlab或者Jenkins或者其他工具）触发的时候，你的源代码会被拉取到构建服务器上的某个目录，然后工作目录会自动切换到该目录，其实就是远端仓库（gitlab或者GitHub）上的源代码）拷贝到nodejs镜像刚才创建的文件夹中
COPY . /var/www/frontEnd/
# 切换镜像中的工作目录
WORKDIR /var/www/frontEnd
# 在该目录中运行这两个命令，将代码打包
RUN npm install && npm run build
# 运行环境的镜像
FROM nginx:latest
LABEL maintainer="1986tianxie@sina.com"
# 在nginx镜像中创建一个文件夹
WORKDIR /project
# 删除nginx的默认配置
RUN rm /etc/nginx/conf.d/default.conf
# 这里的nodeImage就是上面第一句的别名（Docker中Dockerfile多From 指令存在的意义.note），多FROM，参考：https://www.cnblogs.com/leoyang63/articles/13733967.html
COPY --from=nodeImage /var/www/frontEnd/dist /project
# 复制我们项目中的nginx配置(参考：https://hub.docker.com/_/nginx中的conplex configuration)我的nginx配置文件.note
COPY ./nginx.conf /etc/nginx/nginx.conf
# expose,参考Dockerfile指令详解.note
EXPOSE 3300
# 启动nginx服务
ENTRYPOINT ["nginx", "-g", "daemon off;"]